<?php

namespace App\Http\Controllers;

use App\Models\Resultado;
use App\Models\Prova;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResultadosController extends Controller
{
    public function idade()
    {
        $sql = "SELECT
            CASE
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) < 18 THEN '18'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 18 AND 25 THEN '18 - 25'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 25 AND 35 THEN '25 - 35'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 35 AND 45 THEN '35 - 45'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) BETWEEN 45 AND 55 THEN '45 - 55'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) >= 55 THEN 'Acima de 55'
                WHEN YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))) IS NULL THEN ''
            END AS colocacoes
        FROM resultados r
        LEFT join corredores_corridas cc on cc.id = r.corredores_corridas_id
        LEFT join provas p on p.id = r.provas_id
        ORDER BY p.id, YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento)));";

        $resultados = DB::select(DB::raw($sql));
        $listaProvas = Prova::get();

        $dados = [];
        foreach ($resultados as $value) {
            foreach ($listaProvas as $prova) {

                switch ($value->colocacoes) {
                    case '18 - 25':
                        $query = Resultado::getPorOrdemIdade(18, 25, $prova);
                        // dd($query);
                        foreach ($query as $idade) {
                            $dados[] = $idade;
                        }
                        break;

                    case '25 - 35':
                        $query = Resultado::getPorOrdemIdade(25, 35, $prova);

                        foreach ($query as $idade) {
                            $dados[] = $idade;
                        }
                        break;

                    case '35 - 45':
                        $query = Resultado::getPorOrdemIdade(35, 45, $prova);

                        foreach ($query as $idade) {
                            $dados[] = $idade;
                        }
                        break;
                    case '45 - 55':
                        $query = Resultado::getPorOrdemIdade(45, 55, $prova);

                        foreach ($query as $idade) {
                            $dados[] = $idade;
                        }
                        break;
                    case 'Acima de 55':
                        $query = Resultado::getPorOrdemIdade(55, 100, $prova);

                        foreach ($query as $idade) {
                            $dados[] = $idade;
                        }
                        break;
                }
            }
        }


        $geral = [];
        foreach ($dados as $value) {
            $geral[] = $value;
        }

        return response()->json($geral);
    }

    public function geral()
    {
        $sql = "SELECT
            @posicao:=
            CASE
                WHEN @grupo<> r.provas_id THEN 1
                WHEN CAST(@tempo_gasto AS TIME) = r.hr_conclusao_prova THEN @posicao
                ELSE @posicao + 1
            END
            AS Posicao,
        @grupo := r.provas_id as 'ID da prova',
        CONCAT(p.tipo_prova,' Km') as 'Tipo de prova',
        r.corredores_corridas_id as 'ID do corredor',
        CONCAT(YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(cc.dt_nascimento))),' anos') AS Idade,
        cc.nome AS 'Nome do corredor'
        FROM resultados r
        LEFT join corredores_corridas cc on cc.id = r.corredores_corridas_id
        LEFT join provas p on p.id = r.provas_id
        ORDER BY r.provas_id, r.hr_conclusao_prova;";

        DB::statement("SET @posicao:=0;");
        DB::statement("SET @grupo:='';");
        DB::statement("SET @tempo_gasto:=cast('00:00:00' AS time);");

        $resultados = DB::select(DB::raw($sql));
        return response()->json($resultados);
    }

    public function store(Request $request)
    {
        $resultado = new Resultado();
        $resultado->fill($request->all());
        $resultado->save();

        return response()->json($resultado, 201);
    }
}
