<?php

namespace App\Http\Controllers;

use App\Models\CorredorCorrida;
use App\Models\CorredorProva;
use App\Models\Prova;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class CorredoresController extends Controller
{
    public function index()
    {
        $corredores = CorredorCorrida::get();
        return response()->json($corredores);
    }

    public function store(Request $request)
    {
        $idade = Carbon::parse(Carbon::createFromFormat('d/m/Y', $request->dt_nascimento))->diff(Carbon::now())->format('%y');

        if ($idade < 18) {
            return response()->json([
                'message'   => "Não é permitida a inscrição de menores de idade",
            ], 404);
        }


        $prova = Prova::find($request->prova)
            ->join('corredores_provas', 'provas.id', '=', 'corredores_provas.provas_id')
            ->join('corredores_corridas', 'corredores_provas.corredores_corridas_id', '=', 'corredores_corridas.id')
            ->where('corredores_corridas.cpf', $request->cpf)->first();

        if ($prova) {
            return response()->json([
                'message'   => "O corredor $request->nome não pode estar cadastrado nas provas de corridas no dia $prova->data",
            ], 404);
        }

        $corredor = new CorredorCorrida();
        $corredor->fill($request->all());
        $corredor->save();

        DB::table('corredores_provas')->insert([
            'corredores_corridas_id' => $corredor->id,
            'provas_id' => $request->prova,
        ]);

        return response()->json($corredor, 201);
    }

    public function show($id)
    {
        $corredor = CorredorCorrida::find($id);

        if (!$corredor) {
            return response()->json([
                'message'   => 'Registro não encontrado',
            ], 404);
        }

        return response()->json($corredor);
    }

    public function update(Request $request, $id)
    {

        $idade = Carbon::parse(Carbon::createFromFormat('d/m/Y', $request->dt_nascimento))->diff(Carbon::now())->format('%y');

        if ($idade < 18) {
            return response()->json([
                'message'   => "Não é permitida a inscrição de menores de idade",
            ], 404);
        }

        $prova = Prova::find($request->prova)
            ->join('corredores_provas', 'provas.id', '=', 'corredores_provas.provas_id')
            ->join('corredores_corridas', 'corredores_provas.corredores_corridas_id', '=', 'corredores_corridas.id')
            ->where('corredores_corridas.cpf', $request->cpf)->first();

        if ($prova) {
            return response()->json([
                'message'   => "O corredor $request->nome não pode estar atualizado nas provas de corridas no dia $prova->data",
            ], 404);
        }

        $corredor = CorredorCorrida::find($id);

        if (!$corredor) {
            return response()->json([
                'message'   => 'Registro não encontrado',
            ], 404);
        }

        $corredor->fill($request->all());
        $corredor->save();

        DB::table('corredores_provas')
            ->where('corredores_corridas_id', $corredor->id)
            ->where('provas_id', $request->prova)
            ->delete();

        DB::table('corredores_provas')->insert([
            'corredores_corridas_id' => $corredor->id,
            'provas_id' => $request->prova,
        ]);

        return response()->json($corredor);
    }

    public function destroy($id)
    {
        $corredor = CorredorCorrida::find($id);

        if (!$corredor) {
            return response()->json([
                'message'   => 'Registro não encontrado',
            ], 404);
        }

        $corredor->delete();
    }
}
