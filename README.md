# Desafio - Deliver - Laravel 8.35.1
Desenvolver um serviço REST

## Servidor:
Nginx 1.15.0 / PHP 7.3.6 / MySQL 8.0.23

## Instalação

~~~~
git clone https://gitlab.com/rjcfj/deliver.git
composer update
php -r "file_exists('.env') || copy('.env.example', '.env');"
php artisan key:generate

~~~~

## Docker

Passo 1:
Instalar o docker compose na sua maquina.

Passo 2:
Clonar Laradock no seu Projeto laravel.
~~~~
$ git clone https://github.com/Laradock/laradock.git
~~~~

Passo 3:
Entra na pasta do Laradock renomeia o arquivo env-example por .env.
~~~~
$ cp env-example .env
~~~~

Passo 4:
Run
~~~~
$ docker-compose up -d nginx mysql
~~~~
Isto vai levar algum tempo para criar as imagens na tua maquina.

Passo 5:
Configura o teu projeto
~~~~
$ docker-compose exec workspace bash
$ php artisan migrate
~~~~

Depois disso, tudo deve funcionar de forma normal.

Inicie um servidor de desenvolvimento local é, visite http://localhost

## API (JSON)
Serviço Rest: Postman ou Insomnia 

## GET (Lista)

#### Corredor
http://localhost/api/corredores

#### Provas
http://localhost/api/provas

#### Gerais
http://localhost/api/gerais

#### Idades
http://localhost/api/idades

## GET (Buscar)

#### Corredor
http://localhost/api/corredores/1

#### Provas
http://localhost/api/provas/1

## POST

#### Corredor
http://localhost/api/corredores
~~~~
{"nome":"$","cpf":"###.###.###-##","dt_nascimento":"##/##/####","prova":"#"}
~~~~

#### Provas
http://localhost/api/provas
~~~~
{"tipo_prova":"#","data":"##/##/####"} 
~~~~

#### Resultado
http://localhost/api/resultado
~~~~
{"corredores_corridas_id":"#","provas_id":"#", "hr_inicio_prova":""##:##:##"", "hr_conclusao_prova":""##:##:##""} 
~~~~

## PUT

#### Corredor
http://localhost/api/corredores
~~~~
{"nome":"$","cpf":"###.###.###-##","dt_nascimento":"##/##/####","prova":"#"}
~~~~

#### Provas
http://localhost/api/provas
~~~~
{"tipo_prova":"#","data":"##/##/####"} 
~~~~

## DELETE

#### Corredor
http://localhost/api/corredores

#### Provas
http://localhost/api/provas
